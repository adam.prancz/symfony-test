<?php

namespace App\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class RequestResetPasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // TODO: fix username equals email issue
            ->add('_username', null, [
                'label' => 'user.email',
                'error_bubbling' => true,
                'constraints' => [new NotBlank(), new Email()],
                'translation_domain' => 'forms'
            ]);
    }
}