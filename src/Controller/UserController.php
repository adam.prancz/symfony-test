<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\User\EditType;
use App\Form\User\RegistrationType;
use App\Form\User\RequestResetPasswordType;
use App\Form\User\ResetPasswordType;
use App\Security\LoginFormAuthenticator;
use App\Service\CaptchaValidator;
use App\Service\Mailer;
use App\Service\TokenGenerator;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @Route("/user", name="user_")
 */
class UserController extends AbstractController
{
    const DOUBLE_OPT_IN = false;
    const REGISTER_REDIRECT = '/user';

    /**
     * @Route("", name="user")
     * @param Request $request
     *
     * @return Response
     */
    public function user(Request $request): Response
    {
        return $this->render('user/user.html.twig', [
            'title' => 'user',
            'locale' => $request->getLocale(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/register", name="register")
     * @param Request                      $request
     * @param TokenGenerator               $tokenGenerator
     * @param UserPasswordEncoderInterface $encoder
     * @param Mailer                       $mailer
     * @param CaptchaValidator             $captchaValidator
     * @param TranslatorInterface          $translator
     *
     * @return Response
     * @throws Throwable
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function register(Request $request, TokenGenerator $tokenGenerator, UserPasswordEncoderInterface $encoder,
                             Mailer $mailer, CaptchaValidator $captchaValidator, TranslatorInterface $translator): Response
    {
        if ($this->checkAuth()) {
            return $this->redirect(self::REGISTER_REDIRECT);
        }

        $form = $this->createForm(RegistrationType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();

            try {
                if (!$captchaValidator->validateCaptcha($request->get('g-recaptcha-response'))) {
                    $form->addError(new FormError($translator->trans('captcha.wrong')));
                    throw new ValidatorException('captcha.wrong');
                }

                $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
                $token = $tokenGenerator->generateToken();
                $user->setToken($token);
                $user->setIsActive(false);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                if (self::DOUBLE_OPT_IN) {
                    $mailer->sendActivationEmailMessage($user);
                    $this->addFlash('success', 'user.activation-link');
                    return $this->redirect($this->generateUrl('homepage'));
                }

                return $this->redirect($this->generateUrl('user_activate', ['token' => $token]));

            } catch (ValidatorException $exception) {

            }
        }

        return $this->render('user/register.html.twig', [
            'form' => $form->createView(),
            'captchakey' => $captchaValidator->getKey(),
            'title' => 'user.registration',
            'locale' => $request->getLocale()
        ]);
    }

    /**
     * @Route("/activate/{token}", name="activate")
     * @param Request                   $request
     * @param User                      $user
     * @param GuardAuthenticatorHandler $authenticatorHandler
     * @param LoginFormAuthenticator    $loginFormAuthenticator
     *
     * @return Response
     */
    public function activate(Request $request, User $user, GuardAuthenticatorHandler $authenticatorHandler, LoginFormAuthenticator $loginFormAuthenticator): Response
    {
        $user->setIsActive(true);
        $user->setToken(null);
        $user->setActivatedAt(new DateTime());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $this->addFlash('success', 'user.welcome');

        // automatic login
        return $authenticatorHandler->authenticateUserAndHandleSuccess(
            $user,
            $request,
            $loginFormAuthenticator,
            'main'
        );
    }

    /**
     * @Route("/reset-password/{token}", name="reset_password")
     * @param Request                      $request
     * @param User                         $user
     * @param GuardAuthenticatorHandler    $authenticatorHandler
     * @param LoginFormAuthenticator       $loginFormAuthenticator
     * @param UserPasswordEncoderInterface $encoder
     *
     * @return Response
     */
    public function resetPassword(Request $request, User $user, GuardAuthenticatorHandler $authenticatorHandler,
                                  LoginFormAuthenticator $loginFormAuthenticator, UserPasswordEncoderInterface $encoder): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirect($this->generateUrl('homepage'));
        }

        $form = $this->createForm(ResetPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $user->setToken(null);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'user.update.success');

            // automatic login
            return $authenticatorHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $loginFormAuthenticator,
                'main'
            );
        }

        return $this->render('user/password-reset.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit", name="edit")
     * @Security("has_role('ROLE_USER')")
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $encoder
     *
     * @return Response
     */
    public function edit(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $origPwd = $this->getUser()->getPassword();
        $form = $this->createForm(EditType::class, $this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            /** @var User $user */
            $user = $form->getData();
            $pwd = $user->getPassword() ? $encoder->encodePassword($user, $user->getPassword()) : $origPwd;
            $user->setPassword($pwd);
            $entityManager = $this->getDoctrine()->getManager();

            if ($form->isValid()) {
                $entityManager->persist($user);
                $entityManager->flush();
                $this->addFlash('success', 'user.update.success');

                return $this->redirect($this->generateUrl('homepage'));
            }

            $entityManager->refresh($user);
        }

        return $this->render('user/edit.html.twig', [
            'form' => $form->createView(),
            'title' => 'user.edit',
            'locale' => $request->getLocale()
        ]);
    }

    /**
     * @Route("/request-password-reset", name="request_password_reset")
     * @param Request             $request
     * @param TokenGenerator      $tokenGenerator
     * @param Mailer              $mailer
     * @param CaptchaValidator    $captchaValidator
     * @param TranslatorInterface $translator
     *
     * @return Response
     * @throws Throwable
     */
    public function requestPasswordReset(Request $request, TokenGenerator $tokenGenerator, Mailer $mailer,
                                         CaptchaValidator $captchaValidator, TranslatorInterface $translator): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirect($this->generateUrl('homepage'));
        }

        $form = $this->createForm(RequestResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                if (!$captchaValidator->validateCaptcha($request->get('g-recaptcha-response'))) {
                    $form->addError(new FormError($translator->trans('captcha.wrong')));
                    throw new ValidatorException('captcha.wrong');
                }
                $repository = $this->getDoctrine()->getRepository(User::class);

                /** @var User $user */
                $user = $repository->findOneBy(['email' => $form->get('_username')->getData(), 'isActive' => true]);
                if (!$user) {
                    $this->addFlash('warning', 'user.not-found');
                    return $this->render('user/request-password-reset.html.twig', [
                        'form' => $form->createView(),
                        'captchakey' => $captchaValidator->getKey(),
                        'title' => 'user.password.reset',
                        'locale' => $request->getLocale()
                    ]);
                }

                $token = $tokenGenerator->generateToken();
                $user->setToken($token);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $mailer->sendResetPasswordEmailMessage($user);

                $this->addFlash('success', 'user.request-password-link');
                return $this->redirect($this->generateUrl('homepage'));
            } catch (ValidatorException $exception) {

            }
        }

        return $this->render('user/request-password-reset.html.twig', [
            'form' => $form->createView(),
            'captchakey' => $captchaValidator->getKey(),
            'title' => 'user.password.reset',
            'locale' => $request->getLocale()
        ]);
    }

    /**
     * @return bool
     */
    private function checkAuth(): bool
    {
        return $this->isGranted('IS_AUTHENTICATED_FULLY') || $this->isGranted('IS_AUTHENTICATED_REMEMBERED');
    }
}