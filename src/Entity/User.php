<?php

namespace App\Entity;

use App\Validator\Constraints\ComplexPassword;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="user.exists")
 */
class User extends BaseEntity implements UserInterface
{
    /**
     * @ORM\Column(type="string", length=60, unique=true)
     * @Assert\NotBlank(groups={"Registration"}, message="user.email.not_blank")
     * @Assert\Email()
     */
    private $email = '';

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank(groups={"Registration", "PasswordReset"}, message="user.password.not_blank")
     * @Assert\Length(min="8")
     * @ComplexPassword()
     */
    private $password = '';

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $token = '';

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $activatedAt = null;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $nome
     */
    public function setName($nome): void
    {
        $this->name = $nome;
    }

    /**
     * @return mixed
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @param mixed $activatedAt
     *
     * @return $this
     */
    public function setActivatedAt($activatedAt): self
    {
        $this->activatedAt = $activatedAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->getEmail();
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    // TODO use them

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return array|string[]
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->isActive;
    }

    /**
     * @return string
     *
     * @see \Serializable::serialize()
     */
    public function serialize(): string
    {
        return serialize([
            $this->id,
            $this->email,
            $this->name,
            $this->isActive,
            $this->password,
            $this->roles
        ]);
    }

    /**
     * @param string $serialized
     *
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        [
            $this->id,
            $this->email,
            $this->name,
            $this->isActive,
            $this->password,
            $this->roles
        ] = unserialize($serialized);
    }

    /**
     * @return mixed
     */
    public function getActivatedAt()
    {
        return $this->activatedAt;
    }
}