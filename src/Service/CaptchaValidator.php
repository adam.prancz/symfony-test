<?php

namespace App\Service;

use ReCaptcha\ReCaptcha;

class CaptchaValidator
{
    /** @var string */
    private $apiKey;

    /** @var string */
    private $apiSecret;

    /**
     * CaptchaValidator constructor.
     *
     * @param string $apiKey
     * @param string $apiSecret
     */
    public function __construct(string $apiKey = '', string $apiSecret = '')
    {
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
    }

    /**
     * @param $gRecaptchaResponse
     *
     * @return bool
     */
    public function validateCaptcha($gRecaptchaResponse = null): bool
    {
        return true;

        $recaptcha = new ReCaptcha($this->apiSecret);
        $resp = $recaptcha->verify($gRecaptchaResponse);

        return $resp->isSuccess();
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->apiKey;
    }
}