Feature:
  In order to prove that the Behat Symfony extension is correctly installed
  As a user
  I want to have a demo scenario

  Scenario Outline: Logging in
    Given I am on "/login"
    And I fill in "Username" with "<username>"
    And I fill in "Password" with "<password>"
    And I press "Login"
    Then I should see "<content>"

    Examples:
      | username      | password | content |
      | test@test.com | Test0001 | Logout  |
      | test1         | test1    | 15      |